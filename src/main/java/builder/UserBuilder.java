package builder;

import model.Statement;
import model.User;
import properties.ElevatorProperties;

public class UserBuilder {
    private int currentFloor;
    private Statement statement;

    public UserBuilder setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
        return this;
    }

    public UserBuilder setStatement(Statement statement) {
        this.statement = statement;
        return this;
    }

    public User build(){
        User user = new User();
        if(currentFloor == ElevatorProperties.getMinFloor() && statement == Statement.DOWN)
            throw new IllegalArgumentException();
        if(currentFloor == ElevatorProperties.getMaxFloor() && statement == Statement.UP)
            throw new IllegalArgumentException();
        user.setCurrentFloor(currentFloor);
        user.setStatement(statement);
        return user;
    }
}
