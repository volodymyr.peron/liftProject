import builder.ElevatorBuilder;
import builder.UserBuilder;
import model.Elevator;
import model.Statement;
import model.User;
import org.apache.log4j.Logger;
import org.testng.annotations.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;

public class UserTest {

    final static Logger log = Logger.getLogger(UserTest.class.getName());

    @DataProvider(name = "elevatorsDataProvider")
    public Object[][] elevatorsDataProvider() {
        return new Object[][]{
                {
                        new UserBuilder().setCurrentFloor(3).setStatement(Statement.UP).build(),
                        Arrays.asList(
                                new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(100).setFinishFloor(2).build(),
                                new ElevatorBuilder().setStatement(Statement.UP).setCurrentFloor(1).setFinishFloor(2).build()
                        ),
                        new ElevatorBuilder().setStatement(Statement.UP).setCurrentFloor(1).setFinishFloor(2).build()
                },
                {
                        new UserBuilder().setCurrentFloor(4).setStatement(Statement.DOWN).build(),
                        Arrays.asList(
                                new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(100).setFinishFloor(2).build(),
                                new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(5).setFinishFloor(3).build(),
                                new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(7).setFinishFloor(4).build()
                        ),
                        new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(5).setFinishFloor(3).build()
                },
                {
                        new UserBuilder().setCurrentFloor(4).setStatement(Statement.DOWN).build(),
                        Arrays.asList(
                                new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(100).setFinishFloor(2).build(),
                                new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(5).setFinishFloor(4).build(),
                                new ElevatorBuilder().setStatement(Statement.UP).setCurrentFloor(2).setFinishFloor(4).build()
                        ),
                        new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(5).setFinishFloor(4).build()
                },
                {
                        new UserBuilder().setCurrentFloor(96).setStatement(Statement.DOWN).build(),
                        Arrays.asList(
                                new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(100).setFinishFloor(99).build(),
                                new ElevatorBuilder().setStatement(Statement.UP).setCurrentFloor(2).setFinishFloor(96).build(),
                                new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(95).setFinishFloor(94).build(),
                                new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(10).setFinishFloor(1).build()
                        ),
                        new ElevatorBuilder().setStatement(Statement.UP).setCurrentFloor(2).setFinishFloor(96).build()
                },
                {
                        new UserBuilder().setCurrentFloor(5).setStatement(Statement.DOWN).build(),
                        Arrays.asList(
                                new ElevatorBuilder().setStatement(Statement.STAY).setCurrentFloor(2).setFinishFloor(2).build(),
                                new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(6).setFinishFloor(3).build(),
                                new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(4).setFinishFloor(2).build()
                        ),
                        new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(6).setFinishFloor(3).build()
                },
                {
                        new UserBuilder().setCurrentFloor(5).setStatement(Statement.UP).build(),
                        Arrays.asList(
                                new ElevatorBuilder().setStatement(Statement.UP).setCurrentFloor(7).setFinishFloor(9).build(),
                                new ElevatorBuilder().setStatement(Statement.UP).setCurrentFloor(3).setFinishFloor(4).build(),
                                new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(10).setFinishFloor(5).build()
                        ),
                        new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(10).setFinishFloor(5).build()
                },
                {
                        new UserBuilder().setCurrentFloor(5).setStatement(Statement.UP).build(),
                        Arrays.asList(
                                new ElevatorBuilder().setStatement(Statement.UP).setCurrentFloor(7).setFinishFloor(9).build(),
                                new ElevatorBuilder().setStatement(Statement.UP).setCurrentFloor(6).setFinishFloor(8).build(),
                                new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(4).setFinishFloor(3).build(),
                                new ElevatorBuilder().setStatement(Statement.UP).setCurrentFloor(2).setFinishFloor(3).build()
                        ),
                        new ElevatorBuilder().setStatement(Statement.DOWN).setCurrentFloor(4).setFinishFloor(3).build()
                }
        };
    }

    @Test(dataProvider = "elevatorsDataProvider")
    public void getElevatorTest(User user, List<Elevator> elevators, Elevator expectedElevator) {
        long startTime = System.nanoTime();
        assertEquals(user.getElevator(elevators), expectedElevator);
        long endTime = System.nanoTime();
        log.info("Test:\n" + user + "\n"
                + "Elevators: " + elevators + "\n"
                + "Expected elevator: " + expectedElevator + "\n" + "Time = " + (endTime - startTime) + "nano");
    }
}
