package model;

import java.util.Comparator;
import java.util.List;

/**
 * Created by vperotc on 6/25/2018.
 */
public class User {
    private int currentFloor;
    private Statement statement;

    public User() {
        //empty constructor
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    public Elevator getElevator(List<Elevator> elevators) {
        if (statement == Statement.DOWN) {
            return elevators.stream()
                    .filter(l -> (l.getStatement() == Statement.DOWN) && (l.getFinishFloor() <= currentFloor && l.getCurrentFloor() >= currentFloor))
                    .min(Comparator.comparing(Elevator::getCurrentFloor))
                    .orElse(elevators.stream()
                            .filter(l -> l.getFinishFloor() == currentFloor && l.getStatement() != Statement.STAY)
                            .min(Comparator.comparing(l -> (Math.abs(l.getCurrentFloor() - l.getFinishFloor()) + Math.abs(l.getFinishFloor() - currentFloor))))
                            .orElse(elevators.stream()
                                    .filter(l -> l.getStatement() == Statement.STAY && l.getCurrentFloor() == currentFloor)
                                    .findAny()
                                    .orElse(elevators.stream()
                                            .min(Comparator.comparing(l -> Math.abs(l.getFinishFloor() - l.getCurrentFloor()) + Math.abs(l.getFinishFloor() - currentFloor)))
                                            .orElse(null)
                                    )
                            )
                    );
        }
        return elevators.stream()
                .filter(l -> (l.getStatement() == Statement.UP) && (l.getFinishFloor() >= currentFloor && l.getCurrentFloor() <= currentFloor))
                .max(Comparator.comparing(Elevator::getCurrentFloor))
                .orElse(elevators.stream()
                        .filter(l -> l.getFinishFloor() == currentFloor && l.getStatement() != Statement.STAY)
                        .min(Comparator.comparing(l -> Math.abs(l.getCurrentFloor() - l.getFinishFloor()) + Math.abs(l.getFinishFloor() - currentFloor)))
                        .orElse(elevators.stream()
                                .filter(l -> l.getStatement() == Statement.STAY && l.getCurrentFloor() == currentFloor)
                                .findAny()
                                .orElse(elevators.stream()
                                        .min(Comparator.comparing(l -> Math.abs(l.getFinishFloor() - l.getCurrentFloor()) + Math.abs(l.getFinishFloor() - currentFloor)))
                                        .orElse(null)
                                )
                        )
                );
    }
    @Override
    public String toString() {
        return "User{" +
                "currentFloor=" + currentFloor +
                ", statement=" + statement +
                '}';
    }
}
