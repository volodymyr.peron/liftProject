package model;

/**
 * Created by vperotc on 6/25/2018.
 */
public enum Statement {
    UP, DOWN, STAY
}
