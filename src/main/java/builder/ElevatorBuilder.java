package builder;

import model.Elevator;
import model.Statement;

public class ElevatorBuilder {
    private Statement statement;
    private int currentFloor;
    private int finishFloor;

    public ElevatorBuilder setStatement(Statement statement) {
        this.statement = statement;
        return this;
    }

    public ElevatorBuilder setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
        return this;
    }

    public ElevatorBuilder setFinishFloor(int finishFloor) {
        this.finishFloor = finishFloor;
        return this;
    }

    public Elevator build(){
        Elevator elevator = new Elevator();
        if(statement == Statement.DOWN && finishFloor >= currentFloor)
            throw new IllegalArgumentException();
        if(statement == Statement.UP && finishFloor <= currentFloor)
            throw new IllegalArgumentException();
        if(statement == Statement.STAY && finishFloor != currentFloor)
            throw new IllegalArgumentException();
        elevator.setStatement(statement);
        elevator.setCurrentFloor(currentFloor);
        elevator.setFinishFloor(finishFloor);
        return elevator;
    }
}
