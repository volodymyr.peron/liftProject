package properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ElevatorProperties {
    private static Properties properties = new Properties();

    private ElevatorProperties() {
        throw new IllegalArgumentException("Utility class");
    }

    public static int getMaxFloor(){
        int maxFloor = 0;
        try (InputStream inputStream = new FileInputStream("src/main/resources/config.properties")) {
            properties.load(inputStream);
            maxFloor = Integer.parseInt(properties.getProperty("maximumFloor"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return maxFloor;
    }
    public static int getMinFloor(){
        int minFloor = 0;
        try(InputStream inputStream = new FileInputStream("src/main/resources/config.properties")) {
            properties.load(inputStream);
            minFloor = Integer.parseInt(properties.getProperty("minimumFloor"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return minFloor;
    }
}
