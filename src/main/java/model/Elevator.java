package model;

/**
 * Created by vperotc on 6/25/2018.
 */
public class Elevator {
    private Statement statement;
    private int currentFloor;
    private int finishFloor;

    public Elevator() {
        //empty constructor
    }


    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }

    public int getFinishFloor() {
        return finishFloor;
    }

    public void setFinishFloor(int finishFloor) {
        this.finishFloor = finishFloor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Elevator elevator = (Elevator) o;

        if (currentFloor != elevator.currentFloor) return false;
        if (finishFloor != elevator.finishFloor) return false;
        return statement == elevator.statement;
    }

    @Override
    public int hashCode() {
        int result = statement != null ? statement.hashCode() : 0;
        result = 31 * result + currentFloor;
        result = 31 * result + finishFloor;
        return result;
    }

    @Override
    public String toString() {
        return "Elevator{" +
                "statement=" + statement +
                ", currentFloor=" + currentFloor +
                ", finishFloor=" + finishFloor +
                '}';
    }
}
